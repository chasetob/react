/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a neccessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import 'whatwg-fetch';
import store from './store';

export default class HomePage extends React.Component {
    render() {
    var BankingData = React.createClass({

     getInitialState: function() {
     return {
       StatusCode : undefined,
       Err : undefined,
       Message  : undefined,
       Meta : undefined,
       BankingData: undefined
     };
     },
      
      componentDidMount: function() {

        this.serverRequest = fetch(store().url)
            .then(function (res) {
              return res.json()
            }).then(function (result) {
              this.setState({
                StatusCode : result.statusCode,
                Err   : result.error,
                Message : result.message,
                Meta : result.meta,
                BankingData: result.data
              });
              //console.log(this.state.StatusCode);
              //console.log(result.error);
              //console.log(result.message);
              //console.log(this.state.BankingData);
            }.bind(this));
      },


      render: function() {
        return (
            <Sum StatusCode={this.state.StatusCode} Error={this.state.Err} Message={this.state.Message} BankingData={this.state.BankingData}/>
        );
      }
    });

      var Status = React.createClass({
        render: function() {
          return (
              <div><span>StatusCode:</span><span>{this.props.StatusCode}</span></div>
          );
        }
      });
      var Err = React.createClass({
        render: function() {
          return (
              <div><span>{this.props.Error}</span></div>
          );
        }
      });
      var Message = React.createClass({
        render: function() {
          return (
              <div><span>{this.props.Message}</span></div>
          );
        }
      });

      var Sum = React.createClass({
        render: function() {
          var view = [];
          view.push(<Status StatusCode = {this.props.StatusCode} />);
          view.push(<Err Error = {this.props.Err} />);
          view.push(<Message Message = {this.props.Message} />);
          view.push( <BankingInfo  Bank={this.props.BankingData} />);
          return (
              <div>{view}</div>
          );
        }
      });

    var BankingInfo = React.createClass({
      render: function() {
        var line = [];
        this.props.Bank.forEach(function(dt) {
          line.push(<MajorLine bankingData={dt} />);
        }.bind(this));

        return (
           <div>
             {line}
           </div>
        );
      }
    });

    var MajorLine = React.createClass({
      render: function() {

        var subline = <SubLine fees={this.props.bankingData.fees} />;

        return (
            <ul>
              <li><span>Bank Name: </span><span>{this.props.bankingData.bank_name}</span></li>
              <li><span>Product ID: </span><span>{this.props.bankingData.product_id}</span></li>
              <li><span>Product Name: </span><span>{this.props.bankingData.product_name}</span></li>
              <li><span>Product Type: </span><span>{this.props.bankingData.product_loan_type}</span></li>
              <li><span>Rate: </span><span>{this.props.bankingData.interest_rate}</span></li>
              <li>{subline}</li>
              <br/>
            </ul>
        );
      }
    });

      var SubLine = React.createClass({
        render: function(){
          return(
              <ul>
                <li><span>league: </span><span>{this.props.fees.upfront.legal}</span></li>
                <li><span>annual: </span><span>{this.props.fees.ongoing.annual}</span></li>
              </ul>
          );
        }
      });

    return (
        <BankingData/>
    );
  }
}